import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject } from 'rxjs';

import { KeycloakProfile } from 'keycloak-js';
import { KeycloakService } from 'keycloak-angular';
import { ApiService } from '_services/api.service';
import { AlertService } from './alert.service';

@Injectable()
export class AuthenticationService {

    public isAuthenticated: boolean;
    public isInitialized = new ReplaySubject<boolean>();

    public userDetails: KeycloakProfile;

    constructor(
      private http: HttpClient,
      private keycloakService: KeycloakService,
      private apiService: ApiService,
      private alertService: AlertService,
    ) {
      this.isAuthenticated = false;
    }

    async init() {
      await this.checkLogin();
      if (this.isAuthenticated) {
        await this.getUserInfo();
        let observeResponse = true;
        this.apiService.getAccount(observeResponse).subscribe(
          response => {
            this.isInitialized.next(true);
          },
          error => {
            if (error.status === 404) {
              this.apiService.createAccount().subscribe(
                async success => {
                  this.alertService.action({
                    data: {
                      message: 'We have started a Profile for you. Welcome!',
                      // action: {
                      //   text: 'Go to Profile',
                      //   navigateTo: `/profile`,
                      // },
                    }
                  });
                  this.isInitialized.next(true);
                },
                error => {
                  console.log("Error when calling createAccount(): ", error)
                }
              );
            }
          }
        );
      }
    }

    initStatus() {
      return this.isInitialized;
    }

    async checkLogin() {
      this.isAuthenticated = await this.keycloakService.isLoggedIn();
    }

    async getUserInfo() {
      this.userDetails = await this.keycloakService.loadUserProfile();
    }

    isUserInRole(role: string): boolean {
      return this.keycloakService.isUserInRole(role);
    }
    
    login() {
      this.keycloakService.login();
    }

    logout() {
      this.keycloakService.logout();
    }

    account() {
      this.keycloakService.getKeycloakInstance().accountManagement();
    }
}
