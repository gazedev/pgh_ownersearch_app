import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BoardsService {

  constructor() {

  }

  getBoards() {
    console.log('getBoards');
    let storage = localStorage.getItem('boards');
    console.log('in storage', storage, (storage===null), 'end');
    let boards = {};
    if (storage===null) {
      // we prefix with 'b' because objects don't respect numerical key insertion order, even if they are added as a string
      boards["b0"] = this._newBoard('Default');
      console.log('boards after new', boards);
      this.saveBoards(boards);
    } else {
      boards = JSON.parse(storage);
    }
    return boards;
  }

  _newBoard(name) {
    return {
      name: name,
      terms: {},
      saves: [],
      searches: [],
    }
  }

  _lastKeyIncrement() {

  }

  addNewBoard(name) {
    let boards = this.getBoards();
    let keys = Object.keys(boards);
    let lastKey = keys[keys.length-1];
    let lastIndex = Number(lastKey.slice(1));
    let newIndex = "b" + (lastIndex+1);
    boards[newIndex] = this._newBoard(name);
    this.saveBoards(boards);
    return boards;
  }

  getLastBoardKey() {
    let boards = this.getBoards();
    let keys = Object.keys(boards);
    let lastKey = keys[keys.length-1];
    return lastKey;
  }

  saveBoards(boards) {
    localStorage.setItem('boards', JSON.stringify(boards));
  }

  saveBoard(id, board) {
    let storage = localStorage.getItem('boards');
    let boards = JSON.parse(storage);
    boards[id] = board;
    localStorage.setItem('boards', JSON.stringify(boards));
  }

}
