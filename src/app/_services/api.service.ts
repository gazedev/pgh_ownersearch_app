import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { emptyish, removeEmptyish } from '_helpers/emptyish';

import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { KeycloakService } from 'keycloak-angular';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public apiUrl: any = {};

  public firstPage: string = "";
  public prevPage: string = "";
  public nextPage: string = "";
  public lastPage: string = "";


  constructor(
    private httpClient: HttpClient,
    private keycloak: KeycloakService,
  ) {

  }
  // service: corp | owner
  setUrl(service: string, url:string) {
    if (!(service == 'corp' || service == 'owner' || service == 'ownersearch')) {
      return null;
    }
    this.apiUrl[service] = url;
  }

  getUrl() {
    return this.apiUrl;
  }

  /*
  * Backend Status
  */

  checkStatus() {
    return this.httpClient.get<any>(`${this.apiUrl.corp}/`);
  }

  /*
  * Account Methods
  */

  getAccount(observeResponse: boolean = false) {
    if (observeResponse) {
      return this.httpClient.get<any>(`${this.apiUrl.ownersearch}/accounts`, {observe: 'response'});
    } else {
      return this.httpClient.get<any>(`${this.apiUrl.ownersearch}/accounts`);
    }
  }

  createAccount() {
    return this.httpClient.post<any>(`${this.apiUrl.ownersearch}/accounts`, '');
  }

  patchAccount(accountId, account) {
    let patchAccount = removeEmptyishFromObjectRecursive(account);
    return this.httpClient.patch<any>(`${this.apiUrl.ownersearch}/accounts/${accountId}`, patchAccount);
  }

  /*
  * Properties
  */
  getProperties(params) {
    params = removeEmptyish(params);
    console.log('params', params);
    return this.httpClient.get<any>(`${this.apiUrl.owner}/properties`, {params: params});
    // return this.httpClient.get<any>(`${this.apiUrl.owner}/properties`);
  }

  /*
  * Enumeration Methods
  */

  closeAllBrowserTabs() {
    return this.httpClient.delete<any>(`${this.apiUrl.corp}/browser/tabs`);
  }

  startEnumeration(details) {
    /*
      details: {
        start:
        end:
        queues:
      }
    */
    return this.httpClient.post<any>(`${this.apiUrl.corp}/corpsearch/enumeration?start=${details.start}&end=${details.end}&queues=${details.queues}`, '');
  }

  resumeEnumeration(searchId) {
    return this.httpClient.patch<any>(`${this.apiUrl.corp}/searches/${searchId}/resume`, '');
  }

  /*
    searchId: guid,
    action: 'stop' | 'resume'
   */
  enumerationAction(searchId, action) {
    return this.httpClient.patch<any>(`${this.apiUrl.corp}/searches/${searchId}/action`, {action: action});
  }

  getSearches() {
    return this.httpClient.get<any>(`${this.apiUrl.corp}/searches`);
  }

  getCorpsLive(params) {
    return this.httpClient.get<any>(`${this.apiUrl.corp}/corpsearch/live`, {
      params: params,
    });
  }

  searchCorps(params) {
    params = removeEmptyish(params);
    return this.httpClient.get<any>(`${this.apiUrl.corp}/corps/search`, {
      params: params,
    });
  }

  /*
  * Tabs Methods
  */

  /*
    TODO: polling functionality for tabs (check to see if any have
    title: "Runtime Error"). ability to kill off tabs individually
   */

  /*
  * Websocket Methods
  */

  public webSockets: any = {};
  _webSocket(path) {
    let apiUrl = new URL(this.apiUrl.corp);
    if (apiUrl.protocol == 'https:') {
      apiUrl.protocol = 'wss:';
    } else {
      apiUrl.protocol = 'ws:';
    }
    let base = apiUrl.origin;
    if (!this.webSockets.hasOwnProperty(path)) {
      this.webSockets[path] = webSocket(base + path);
    }
    return this.webSockets[path];
  }

  // async _wsAuthedNext() {
  //   let sendObject = {};
  //   let authenticated = await this.keycloak.isLoggedIn();
  //   if (authenticated === true) {
  //     const token: string = await this.keycloak.getToken();
  //     sendObject[this.authorizationHeaderName] = this.bearerPrefix + token;
  //   }
  // }

  async _wsConditionallyAuthedNext(endpoint) {
    let sendObject = {};
    const authorizationHeaderName = 'Authorization';
    const bearerPrefix = 'bearer ';
    let authenticated = await this.keycloak.isLoggedIn();
    if (authenticated === true) {
      const token: string = await this.keycloak.getToken();
      sendObject[authorizationHeaderName] = bearerPrefix + token;
    }
    this._webSocket(endpoint).next(sendObject);
  }

  getEvents() {
    this._wsConditionallyAuthedNext('/ws/getSearchEvents');
    return this._webSocket('/ws/getSearchEvents').asObservable();
  }

  /* Ownersearch API */

  getPlaintiffClusters() {
    return this.httpClient.get<any>(`${this.apiUrl.ownersearch}/airtable/plaintiff-clusters`);
    // return this.httpClient.get<any>(`${this.apiUrl.owner}/properties`);
  }

  getBoards() {
    return this.httpClient.get<any>(`${this.apiUrl.ownersearch}/boards`);
  }

  getBoard(boardId) {
    return this.httpClient.get<any>(`${this.apiUrl.ownersearch}/boards/${boardId}`);
  }

  addBoard(board) {
    return this.httpClient.post<any>(`${this.apiUrl.ownersearch}/boards`, board);
  }

  patchBoard(boardId, board) {
    return this.httpClient.patch<any>(`${this.apiUrl.ownersearch}/boards/${boardId}`, board);
  }

  /* WPRDC Parcel Api */

  getParcelRelatedInfo(parcelIds: string[]) {
    return this.httpClient.get<any>(`https://parcel-address-api.pittsburghhousing.org/parcels/info?parcelPin=${parcelIds.join('&parcelPin=')}`);
  }

}

// TODO: make this so it can remove emptyish arrays as well. Look at stackoverflow that uses lodash
function removeEmptyishFromObjectRecursive(value) {
  // iterate through passed in value
  // assume we are passed an object to start
  // if value[i] is type object, recurse
  // save response of recurse function
  // keep value if returned value isn't false
  // if value[i] is not type object, add it to a holding object and return at end
  if (Object.keys(value).length == 0) {
    return null;
  }
  let builder = {};
  for (var key in value) {
    if (typeof value[key] == 'object') {
      let result = removeEmptyishFromObjectRecursive(value[key]);
      if (result !== null) {
        builder[key] = result;
      }
    } else {
      if (
        emptyish(value[key])
        || value[key] == undefined
      ) {
        continue;
      }
      builder[key] = value[key];
    }
  }
  if (Object.keys(builder).length == 0) {
    return null;
  }
  return builder;
}
