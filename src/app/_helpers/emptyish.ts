export function emptyish(value) {
  if (
    value === null ||
    value === ''
  ) {
    return true;
  } else {
    return false;
  }
}

// de-lodashed and simplified version of https://stackoverflow.com/a/26202058/2383249
export function removeEmptyish(obj) {
  return function prune(current) {
    for (let key of Object.keys(current)) {
      let value = current[key];
      if (
        (value === undefined) || (value === null) || (typeof value == 'number' && value != +value) ||
        (typeof value == 'string' && (value == '')) ||
        (typeof value == 'object' && Object.keys(prune(value)).length === 0 )
      ) {
        delete current[key];
      }
    };
    // remove any leftover undefined values from the delete
    // operation on an array
    if (Array.isArray(current)) {
      for (let i = current.length-1; i >= 0; i--) {
        if (current[i] === undefined) {
          current.splice(i, 1);
        }
      }
    }
    return current;
  } (JSON.parse(JSON.stringify(obj)));
}
