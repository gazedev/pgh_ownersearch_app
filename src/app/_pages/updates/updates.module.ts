import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UpdatesPage } from './updates.page';

@NgModule({
  imports: [
    // CommonModule,
    RouterModule.forChild([
      {
        path: 'updates',
        component: UpdatesPage,
      }
    ])
  ],
  declarations: [UpdatesPage]
})
export class UpdatesPageModule {}
