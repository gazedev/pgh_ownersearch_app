import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService, ApiService } from '_services/index';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { DialogNewBoardDialog } from '_components/dialog-new-board-dialog/dialog-new-board-dialog';
import { DialogExportResultsDialog } from '_components/dialog-export-results-dialog/dialog-export-results-dialog';
import { DialogParcelsMapDialog } from '_components/dialog-parcels-map-dialog/dialog-parcels-map-dialog';
import { emptyish, removeEmptyish } from '_helpers/emptyish';
import { environment } from '_environment';

@Component({
  selector: 'app-search',
  templateUrl: 'search.page.html',
  styleUrls: ['search.page.scss'],
})
export class SearchPage {
  public env: any = environment;

  @ViewChild('ngFormDirective') formDirective;
  public formProperty: FormGroup;
  public formCorp: FormGroup;
  public submitAttempt: boolean;
  public currentlySubmitting: boolean;
  public searchPropertyBuilderOpen: boolean = false;
  public searchCorpBuilderOpen: boolean = false;

  public formPropertyFields: any = [
    {
      name: 'parcelId',
      type: 'array',
      label: 'ParcelId',
    },
    {
      name: 'address',
      type: 'array',
      label: 'Address',
    },
    {
      name: 'owner',
      type: 'array',
      label: 'Owner',
    },
    {
      name: 'ownerAddress',
      type: 'array',
      label: 'Owner Address',
    },
  ];
  public formCorpFields: any = [
    {
      name: 'name',
      type: 'string',
      label: 'Name',
    },
    {
      name: 'address',
      type: 'string',
      label: 'Address',
    },
  ];

  public boards: any = [];
  public boardId: string;
  public board: any;

  public saveTerms: any = {};

  public prefillValue: string = "";
  // public prefillDestinations: any = {};

  public parcelInfo: any;

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    public apiService: ApiService,
    public authService: AuthenticationService,
  ) {
    this.formProperty = this.formBuilder.group({});
    this.formCorp = this.formBuilder.group({});
    this.addFormFields();

  }

  async ngOnInit() {
    await this.authService.checkLogin();
    this.authService.initStatus().subscribe(isInit => {
      if (isInit === true) {
        this.getBoards();
      }
    });
  }

  addFormFields() {
    for (let field of this.formPropertyFields) {
      this.formProperty.addControl(field.name, new FormArray([
      ]));
      this.addField('property', field.name);
    }
    for (let field of this.formCorpFields) {
      if (field.type == 'array') {
        this.formCorp.addControl(field.name, new FormArray([]));
        this.addField('corp', field.name);
      } else {
        this.formCorp.addControl(field.name, new FormControl(''));
      }
    }
  }

  async doLogin() {
    await this.authService.login();
  }

  openSearch(type) {
    if (type =='property') {
      this.searchPropertyBuilderOpen = true;
    } else if (type == 'corp') {
      this.searchCorpBuilderOpen = true;
    }
  }

  prefillRequest(type, field, value) {
    // type field and value represent the source of the search request.
    // based on this information we need to present the user with options
    // where the search request could be directed.
    // if (type == 'property') {
    //   if (field == 'owner') {
    //     // TODO: change this to where we set an object of possible destinations
    //     // with highlighted destinations for our suggestion of what is searched
    //     // and build a form based on this object allowing the user to select
    //   }
    // }

    // this will close and clear any search form fields
    this.closeSearch();
    // once there's a prefillValue, a pane opens in the UI to direct the input
    // via the prefillDirector
    this.prefillValue = value;
  }

  prefillDirector(type, field, value) {
    if (type == 'property') {
      this.formProperty.patchValue({[field]: [value]});
      this.searchPropertyBuilderOpen = true;
    } else if (type == 'corp') {
      // don't have a formCorp yet
      this.formCorp.patchValue({[field]: value});
      this.searchCorpBuilderOpen = true;
    }
    this.prefillValue = '';
  }

  prefillCancel() {
    this.prefillValue = '';
  }

  closeSearch() {
    this.searchPropertyBuilderOpen = false;
    this.searchCorpBuilderOpen = false;
    // TODO: clear form inputs
    this.formProperty.reset();
    this.formCorp.reset();
  }

  addField(type, field, value = '') {
    let fields;
    if (type == "property") {
      fields = <FormArray>this.formProperty.get(field);
    } else if (type == "corp") {
      fields = <FormArray>this.formCorp.get(field);
    }
    let fieldItem = new FormControl(value);
    fields.push(<FormControl>fieldItem);
  }

  rmField(type, field, index) {
    let fields;

    if (type == 'property') {
      fields = <FormArray>this.formProperty.get(field);
    } else if (type == 'corp') {
      fields = <FormArray>this.formCorp.get(field);

    }
    fields.removeAt(index);
  }

  submit(type) {
    let params;
    if (type=='property') {
      params = this.formProperty.value;
      this.apiService.getProperties(params).subscribe(res => {
        res.type = 'property';
        console.log('property res', res);
        this.board.searches.push(res);
        this.saveBoard();
        this.formProperty.reset();
        this.closeSearch();
      });
    } else if (type == 'corp') {
      params = this.formCorp.value;
      this.apiService.searchCorps(params).subscribe(res => {
        res.type = 'corp';
        console.log('corp res', res);
        this.board.searches.push(res);
        this.saveBoard();
        this.formCorp.reset();
        this.closeSearch();
      });
    }
    let narrowedParams = removeEmptyish(params);
    for (let field of Object.keys(narrowedParams)) {
      let item = narrowedParams[field];
      if (Array.isArray(item)) {
        for (let value of item) {
          this.setTerm(value, type, field, true);
        }
      } else {
        this.setTerm(item, type, field, true);
      }
    }

  }

  deleteSearch(index) {
    this.board.searches.splice(index, 1);
    this.saveBoard();
  }

  /* Terms Methods */

  setTerm(term, type, field, value) {
    if (!this.board.terms.hasOwnProperty(term)) {
      this.board.terms[term] = {
        property: {
          address: null,
          owner: null,
          ownerAddress: null,
        },
        corp: {
          name: null,
        }
      }

    }
    this.board.terms[term][type][field] = value;
    this.saveBoard();
  }

  clearTerms() {
    this.board.terms = {};
    this.saveBoard();
  }

  /* Saves Methods */

  addToSaves(type, result, saveNow = true) {
    // add a type to all saved results so we can work with them individually
    result.type = type;
    // keep saves de-duplicated, so we check if what we want to save has
    // already been saved
    let found = this.board.saves.find((save) => {
      let typeMatches = (save.type === result.type);
      if (!typeMatches) {
        // Exit early if type doesn't match
        return false;
      }
      let idMatches;
      if (type === 'property') {
        return (save.parid === result.parid);
      } else if (type === 'corp') {
        return (save.details["Entity Number"] === result.details["Entity Number"]);
      } else {
        return false;
      }
    });
    if (found === undefined) {
      this.board.saves.push(result);
      if (saveNow) {
        this.processSavedPropertyFields();
        this.saveBoard();
      }
    }
  }

  deleteSave(index) {
    this.board.saves.splice(index, 1);
    this.processSavedPropertyFields();
    this.saveBoard();
  }

  addAllToSaves(type, results) {
    // we don't want to save after every result. we will save after we add all
    let saveNow = false;
    for (let result of results) {
      this.addToSaves(type, result, saveNow);
    }
    this.processSavedPropertyFields();
    this.saveBoard();
  }

  clearSaves() {
    this.board.saves = [];
    this.processSavedPropertyFields();
    this.saveBoard();
  }

  export(results) {
    const exportDialog = this.dialog.open(DialogExportResultsDialog, {
      data: results,
    });
  }

  processSavedPropertyFields() {
    let saveTerms = {
      owner: {},
      ownerAddress: {},
    };
    let saves = this.board.saves;
    for (let save of saves) {
      if (save.type !== 'property') {
        continue;
      }
      saveTerms['owner'][save.owner] = true;
      saveTerms['ownerAddress'][save.ownerAddress] = true;
    }
    this.saveTerms = saveTerms;
  }

  /* Boards Methods */

  getBoards(switchToBoardId = false) {
    this.apiService.getBoards().subscribe(res => {
      if (res.length ===0) {
        // If no boards, create default and try again
        this.apiService.addBoard({name: 'Default'}).subscribe(() => {
          this.getBoards();
        });
        return null;
      }
      this.boards = res;
      if (switchToBoardId === false) {
        // switch to the first board by default
        this.setActiveBoard(this.boards[0].id);
      } else {
        this.setActiveBoard(switchToBoardId);
      }
    });
  }

  setActiveBoardSelect($event) {
    let value = $event.target.value;
    this.setActiveBoard(value);
  }

  setActiveBoard(id) {
    this.boardId = id;
    this.board = null;
    this.apiService.getBoard(id).subscribe(res => {
      this.board = res;
      this.processSavedPropertyFields();
    });
  }

  openNewBoardDialog() {
    const dialogRef = this.dialog.open(DialogNewBoardDialog);

    dialogRef.afterClosed().subscribe(result => {
      console.log('Dialog result:', result);
      if (result !== undefined) {
        let board: any = {
          name: result.name,
        };
        if (!emptyish(result.airtableRecordId)) {
          board.airtableRecordId = result.airtableRecordId;
        }
        this.apiService.addBoard(board).subscribe(res => {
          // refresh boards list, and pass board id to set it active
          this.getBoards(res.id);
        });
        if (result.addresses.length > 0) {
          this.rmField('property', 'address', 0);
          for (let address of result.addresses) {
            console.log('address', address)
            this.addField('property', 'address', address);
          }
          this.openSearch('property');
        }
      }
    });
  }

  // TODO: When switching to a new board it seems to stick to default board data. The add new field adds index numbers to the fields instead of the actual address info. it also leaves the first field blank (try manually removing a field and then adding from there). When pre-loading address info from a search it should open the property form panel.

  saveBoard() {
    let board: any = {
      name: this.board.name,
      terms: this.board.terms,
      saves: this.board.saves,
      searches: this.board.searches,
    };
    if (!emptyish(this.board.airtableRecordId)) {
      board.airtableRecordId = this.board.airtableRecordId;
    }
    this.apiService.patchBoard(this.board.id, board).subscribe(res => {
      console.log('board saved!');
    });
  }

  /* More Parcel Info Methods */

  getMoreParcelInfo(parid: string) {
    this.parcelInfo = {
      status: 'loading',
      results: {},
    };
    this.apiService.getParcelRelatedInfo([parid]).subscribe(res => {
      if (res.success === true) {
        this.parcelInfo.status = 'ready';
        this.parcelInfo.results = res.results;
      }
    });
  }

  closeMoreParcelInfo() {
    this.parcelInfo = undefined;
  }

  parcelsMapLink(parcelIds) {
    const parcelsQuery = parcelIds.join(',');
    return `https://parcel-address.pittsburghhousing.org?parcels=${parcelsQuery}`;
  }

  openParcelsMapLink($event, parcelIds) {
    $event.preventDefault();
    const mapLink = this.parcelsMapLink(parcelIds);
    const mapDialog = this.dialog.open(DialogParcelsMapDialog, {
      data: mapLink,
    });
  }

  boardSavesPids(saves) {
    const parcels = saves.filter(item => item.type == 'property');
    const parcelIds = parcels.map(parcel => parcel.parid);
    return parcelIds;
  }

}
