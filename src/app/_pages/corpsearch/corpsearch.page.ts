import { Component, ViewChild } from '@angular/core';

import { AuthenticationService, ApiService } from '_services/index';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MatExpansionPanel } from '@angular/material/expansion';

@Component({
  selector: 'app-corpsearch',
  templateUrl: 'corpsearch.page.html',
  styleUrls: ['corpsearch.page.scss'],
})
export class CorpsearchPage {

  @ViewChild('ngFormDirective') formDirective;
  form: FormGroup;
  submitAttempt: boolean;
  currentlySubmitting: boolean;

  @ViewChild('formAccordion') formAccordion: MatExpansionPanel;
  public events: any = {};
  public searches: any = [];

  constructor(
    private formBuilder: FormBuilder,
    public apiService: ApiService,
    public authService: AuthenticationService,
  ) {
    this.form = this.formBuilder.group({
      start: [''],
      end: [''],
      queues: ['1'],
    });
  }

  async ngOnInit() {
    await this.authService.checkLogin();
    console.log('authd', this.authService.isAuthenticated);
    if (this.authService.isAuthenticated) {
      this.getEvents();
      this.getSearches();
    }
  }

  async doLogin() {
    await this.authService.login();
  }

  getSearches() {
    this.apiService.getSearches()
    .subscribe(res => {
      this.searches = res;
    });
  }

  refreshSearches() {
    this.getSearches();
    this.events = {};
    this.getEvents();
  }

  getEvents() {
    // NOTE: This specifically happens when a queue that is still running is "resumed". This might mean that the old tabs aren't being exited, and the old process is still running.
    // something weird can happen where if a search is resumed, its events now have their progress bar based on the start and end of the new queue size (the remainder of the old queue), but if the search is stopped and resumed while the interface is still up, these events can do some sort of thing where old messages and new messages are coming in at the same time (or maybe new messages are coming in but applying to an old interface) and it causes the progress bar to jump around. I think a solution might be to re-build the active search interface when a queue is restarted. not sure. or maybe just merge the active enumerations onto the saved searches interface, provide a little label for what things are receiving messages in the past however long.
    this.apiService.getEvents()
    .subscribe(res => {
      if (res.hasOwnProperty('search')) {
        this.pushEvent(res);
      }
    });
  }

  pushEvent(event) {
    if (!this.events.hasOwnProperty(event.search)) {
      this.events[event.search] = {};
    }
    if (!this.events[event.search].hasOwnProperty(event.queue)) {
      this.events[event.search][event.queue] = {};
    }
    this.events[event.search][event.queue] = {
      status: event.status,
      start: parseInt(event.start),
      end: parseInt(event.end),
      id: parseInt(event.id),
    };
  }

  toString(obj) {
    return JSON.stringify(obj);
  }

  int(str) {
    return parseInt(str);
  }

  clipId(id) {
    return id.substring(0,8);
  }

  roundUp(num) {
    return Math.ceil(num);
  }

  submit() {
    console.log(this.form.value);
    this.apiService.startEnumeration(this.form.value).subscribe(res => {
      console.log('submit response', res);
      this.getSearches();
      this.resetForm();
    });
  }

  resetForm() {
    this.form.reset();
    this.formDirective.resetForm();
    this.formAccordion.close();
  }

  resumeEnumeration(searchId) {
    this.apiService.enumerationAction(searchId, 'resume').subscribe(res => {
      console.log(res);
    });
  }

  stopEnumeration(searchId) {
    this.apiService.enumerationAction(searchId, 'stop').subscribe(res => {
      console.log(res);
    });
  }

  closeAllBrowserTabs() {
    this.apiService.closeAllBrowserTabs().subscribe(res => {
      console.log('DELETE: /browser/tabs');
    });
  }

}
