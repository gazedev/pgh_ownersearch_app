import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpsearchPage } from './corpsearch.page';

describe('CorpsearchPage', () => {
  let component: CorpsearchPage;
  let fixture: ComponentFixture<CorpsearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorpsearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorpsearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
