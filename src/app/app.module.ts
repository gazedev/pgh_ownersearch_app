import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor, ErrorInterceptor } from '_helpers/index';
import { AlertService, AuthenticationService } from '_services/index';
import { ActionSnackBarComponent } from '_components/action-snack-bar/action-snack-bar';
import { DialogNewBoardDialog } from '_components/dialog-new-board-dialog/dialog-new-board-dialog';
import { DialogExportResultsDialog } from '_components/dialog-export-results-dialog/dialog-export-results-dialog';
import { DialogParcelsMapDialog } from '_components/dialog-parcels-map-dialog/dialog-parcels-map-dialog';


import { AngularMaterialModule } from '_components/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
// import { OverlayContainer } from '@angular/cdk/overlay';

import { APP_INITIALIZER } from '@angular/core';
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';
import { initializer } from '_utils/app-init';

@NgModule({
  declarations: [
    AppComponent,
  ],
  entryComponents: [
    ActionSnackBarComponent,
    DialogNewBoardDialog,
    DialogExportResultsDialog,
    DialogParcelsMapDialog,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    KeycloakAngularModule,
  ],
  providers: [
    // AuthGuard,
    AlertService,
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
      deps: [KeycloakService],
    },
    // Use for v9+
    // {
    //   provide: DEFAULT_CURRENCY_CODE,
    //   useValue: 'USD'
    // },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    // overlayContainer: OverlayContainer
  ) {
    // overlayContainer.getContainerElement().classList.add('app-dark');
  }
}
