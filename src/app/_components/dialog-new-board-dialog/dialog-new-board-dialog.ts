import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { ApiService } from '_services/index';

@Component({
  selector: 'dialog-new-board-dialog',
  templateUrl: 'dialog-new-board-dialog.html',
  styleUrls: ['dialog-new-board-dialog.scss'],
})
export class DialogNewBoardDialog {
  public state: string = 'new';
  public plaintiffs: any;
  public formNewBoard: FormGroup;
  public formCustomBoard: FormGroup;
  public response: any;
  public airtableLoading: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<DialogNewBoardDialog>,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
  ) {
    this.formNewBoard = this.formBuilder.group({
      newBoard: ['custom', Validators.required],
      boardName: [''],
    },
    {
      validator: NewBoardBoardNameValidator(),
    });

    this.formCustomBoard = this.formBuilder.group({
      name: ['', Validators.required],
      airtableRecordId: [''],
      addresses: this.formBuilder.array([]),
    });

    this.response = {};
  }

  ngOnInit() {
    this.getPlaintiffClusters();
  }

  getPlaintiffClusters() {
    this.apiService.getPlaintiffClusters().subscribe(response => {
      this.plaintiffs = response;
      this.airtableLoading = false;
      console.log('plaintiffs', this.plaintiffs);
    })
  }

  submitNewBoard() {
    let response: any = {};
    if (this.formNewBoard.value.newBoard == "custom") {
      response.name = this.formNewBoard.value.boardName;
      response.airtableRecordId = '';
      response.addresses = [];
    } else {
      // Airtable
      let recordId = this.formNewBoard.value.newBoard;
      console.log(recordId);
      response.airtableRecordId = recordId;
      let record = this.plaintiffs.find((element) => {
        return (element.id == recordId);
      });
      response.name = record.Name;
      // reset and rebuild array to prevent in-place editing
      response.addresses = [];
      for (let address of record['Plaintiff addresses']) {
        response.addresses.push(address);
      }
    }
    console.log('submitting!', response);
    this.dialogRef.close(response);
  }

  customize() {
    this.state = 'customize';
    let recordId = this.formNewBoard.value.newBoard;
    let record = this.plaintiffs.find((element) => {
      return (element.id == recordId);
    });
    this.formCustomBoard.patchValue({
      name: record.Name,
      airtableRecordId: recordId,
      addresses: record['Plaintiff addresses'],
    });
    this.removeAllAddresses();
    for (let address of record['Plaintiff addresses']) {
      this.addAddress(address);
    }
  }

  removeAllAddresses() {
    for (let i = this.addresses().length; i > 0; i--) {
      this.removeAddress(i-1);
    }
  }

  addresses() {
    return this.formCustomBoard.get('addresses') as FormArray;
  }

  removeAddress(i) {
    this.addresses().removeAt(i);
  }

  addAddress(value = '') {
    let fieldItem = new FormControl(value);
    this.addresses().push(<FormControl>fieldItem);
    console.log('added address', this.addresses());
  }

  back() {
    this.state = 'new';
  }

  submitCustomizedBoard() {
    let response: any = this.formCustomBoard.value;
    console.log('submitting!', response);
    this.dialogRef.close(response);
  }

}

import { ValidatorFn, ValidationErrors, AbstractControl } from "@angular/forms";

export function NewBoardBoardNameValidator(): ValidatorFn {

  return (group: AbstractControl): ValidationErrors | null => {
    let newBoardControl = group.get('newBoard');
    let newBoardValue = newBoardControl.value;

    let boardNameControl = group.get('boardName');
    let boardNameValue = boardNameControl.value;

    boardNameControl.setErrors(null);

    if (newBoardValue == 'custom' && boardNameValue == '') {
      boardNameControl.setErrors({nameNotSet: true});
      return {'nameNotSet': {message: "boardName empty when newBoard set to custom"}};
    }
    return null;
  }
}
