import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '_services/index';

@Component({
  selector: 'dialog-export-results-dialog',
  templateUrl: 'dialog-export-results-dialog.html',
  styleUrls: ['dialog-export-results-dialog.scss'],
})
export class DialogExportResultsDialog {
  public results: any;
  @ViewChild('resultsTextArea') resultsTextArea: ElementRef;
  @ViewChild('fileSaveAnchor') fileSaveAnchor: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<DialogExportResultsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.results = '';
  }

  ngOnInit() {
    console.log('results', this.data);
    let exports = {
      corp: [],
      property: [],
    };
    for (let result of this.data) {
      if (result.type == 'corp') {
        if (exports.corp.length == 0) {
          exports.corp.push(['*Corporations*'])
          exports.corp.push(['name', 'namesHistory', 'details', 'officers']);
        }
        exports.corp.push([
          result.name,
          JSON.stringify(result.namesHistory),
          JSON.stringify(result.details),
          JSON.stringify(result.officers),
        ]);
      }
      if (result.type == 'property') {
        if (exports.property.length == 0) {
          exports.property.push(['*Properties*']);
          exports.property.push(['parid', 'home', 'address', 'muniDesc', 'owner', 'ownerAddress']);
        }
        exports.property.push([
          result.parid,
          result.home,
          result.address,
          result.muniDesc,
          result.owner,
          result.ownerAddress,
        ]);
      }
    }
    let csv = [];
    if (exports.corp.length > 0) {
      let corps = exports.corp.map(corp => {
        return corp.join('\t');
      });
      csv.push(corps.join('\n'));
    }
    if (exports.property.length > 0) {
      let properties = exports.property.map(property => {
        return property.join('\t');
      });
      csv.push(properties.join('\n'));
    }
    this.results = csv.join('\n\n');
  }

  ngOnDestroy() {
    let elem = this.fileSaveAnchor.nativeElement;
    window.URL.revokeObjectURL(elem.href);
  }

  copyResults() {
    this.resultsTextArea.nativeElement.select();
    document.execCommand('copy');
  }

  save() {
    let data = this.results;
    let filename = 'pittsburgh-housing-ownersearch-saved-results.tsv';
    var blob = new Blob([data], {type: 'text/tsv'});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
      var elem = this.fileSaveAnchor.nativeElement;
      if (elem.href) {
        window.URL.revokeObjectURL(elem.href);
        elem.href = '';
      }
      elem.href = window.URL.createObjectURL(blob);
      elem.download = filename;
      elem.click();
    }
  }

}
