import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'dialog-parcels-map-dialog',
  templateUrl: 'dialog-parcels-map-dialog.html',
  styleUrls: ['dialog-parcels-map-dialog.scss'],
})
export class DialogParcelsMapDialog {

  public mapSrc;

  constructor(
    public dialogRef: MatDialogRef<DialogParcelsMapDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public domSanitizer: DomSanitizer,
  ) {

  }

  ngOnInit() {
    this.mapSrc = this.domSanitizer.bypassSecurityTrustResourceUrl(this.data);
  }

  ngOnDestroy() {
  }

}
