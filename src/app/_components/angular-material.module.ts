import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
} from '@angular/material';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ActionSnackBarComponent } from './action-snack-bar/action-snack-bar';
import { DialogNewBoardDialog } from './dialog-new-board-dialog/dialog-new-board-dialog';
import { DialogExportResultsDialog } from './dialog-export-results-dialog/dialog-export-results-dialog';
import { DialogParcelsMapDialog } from './dialog-parcels-map-dialog/dialog-parcels-map-dialog';
import { LandlordCardComponent } from './landlord-card/landlord-card';
import { LoadingBlockComponent } from './loading-block/loading-block';
import { NoContentComponent } from './no-content/no-content';
import { MapBoxComponent } from './map-box/map-box';
import { ReviewCardComponent } from './review-card/review-card';
import { StarsDisplayComponent } from './stars-display/stars-display';

@NgModule({
  declarations: [
    ActionSnackBarComponent,
    DialogNewBoardDialog,
    DialogExportResultsDialog,
    DialogParcelsMapDialog,
    LandlordCardComponent,
    LoadingBlockComponent,
    MapBoxComponent,
    NoContentComponent,
    ReviewCardComponent,
    StarsDisplayComponent,
  ],
  imports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatToolbarModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: 'landlord-card',
        component: LandlordCardComponent,
      },
      {
        path: 'loading-block',
        component: LoadingBlockComponent,
      },
      {
        path: 'no-content',
        component: NoContentComponent,
      },
      {
        path: 'review-card',
        component: ReviewCardComponent,
      },
    ])
  ],
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatToolbarModule,
    ActionSnackBarComponent,
    DialogNewBoardDialog,
    DialogExportResultsDialog,
    DialogParcelsMapDialog,
    LandlordCardComponent,
    LoadingBlockComponent,
    MapBoxComponent,
    NoContentComponent,
    ReviewCardComponent,
    StarsDisplayComponent,
  ],
})
export class AngularMaterialModule {}
